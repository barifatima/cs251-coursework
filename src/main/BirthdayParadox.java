/* Fatima Bari
Reg No: 201902782

Random Date Generate:
https://stackoverflow.com/questions/3985392/generate-random-date-of-birth
*/

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class BirthdayParadox {

    static int matches;
    static int n;

    BirthdayParadox() {
        matches = 0;
    }

    BirthdayParadox(int n) {
        matches = 0;
        this.n = n;
    }

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter n");

        int input = userInput.nextInt();

        new BirthdayParadox(input);

        checkDates();
        double percentage = doPercentage();

        System.out.println("The amount of matches: " + matches);
        System.out.println("The percentage of matches found with " + n + " people is " + percentage + "%");

    }


    private static void checkDates() {
        String testBday;
        ArrayList<String> birthdayArray = new ArrayList<>();
        birthdayArray.add(randomDate());

        for (int i = 0; i < n; i++) {
            testBday = randomDate();

            for (int j =0; j < birthdayArray.size(); j++) {
                if (testBday.equals(birthdayArray.get(j))) {
                    matches = matches + 1;
                }
            }
            birthdayArray.add(testBday);
        }
    }

    private static double doPercentage() {
        return ((float)matches / (float)n) * 100;
    }

    public static String randomDate() {
        GregorianCalendar gc = new GregorianCalendar();
        int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
        String date = gc.get(gc.DAY_OF_MONTH) + "-" + (gc.get(gc.MONTH) + 1);

        return date;
    }

    public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }
}
